package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Random;

public class Generador {
	private final static String EDGES_FILE = "./data/edgesIn.csv";
	private final static String EDGES_OUT = "./data/edgesOut.csv";

	public static void main(String[] args) {
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(EDGES_FILE));
			PrintWriter writer = new PrintWriter(new File(EDGES_OUT));

			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{
				Random random = new Random();
				double cost = 80 + (10000 - 80) * random.nextDouble();
				line+=","+cost;
				line = line.replace(",,", ",");
				line = line.replace(",Y,", ",");
				line = line.replace(",N,", ",");
				if (line.contains(",,")||line.contains(",Y,")||line.contains(",N,")){
					System.out.println("Se va a guardar mal");
				}
				writer.println(line);
			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de arcos: " + e.getMessage());
		}
	}
}
